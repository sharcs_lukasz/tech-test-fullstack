package com.sedex.connect.company

import com.sedex.connect.company.persistence.CompanyRepository
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CompanyApplication

	val repository = CompanyRepository()

fun main(args: Array<String>) {
	runApplication<CompanyApplication>(*args)
}
