package com.sedex.connect.company.controller

import com.sedex.connect.company.model.CompanyRequest
import com.sedex.connect.company.model.CompanyResponse
import com.sedex.connect.company.service.CompanyService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class CompanyController {

    @GetMapping("/healthcheck")
    fun health(): String {
        return( "OK")
    }

    @PostMapping("/company")
    fun addCompany(@RequestBody request: CompanyRequest): ResponseEntity<CompanyResponse> {
        return ResponseEntity<CompanyResponse>(CompanyService.addCompany(request), HttpStatus.OK)
    }
}
