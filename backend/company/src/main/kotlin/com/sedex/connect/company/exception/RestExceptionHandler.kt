package com.sedex.connect.company.exception

import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
class RestExceptionHandler {
    @ExceptionHandler(value=[(IllegalArgumentException::class)])
    fun handleError1(exc: IllegalArgumentException): ResponseEntity<String> {
        if ("Company with this name is already registered".equals(exc.message)) {
            return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .header("X-Reason", exc.message)
                .body(exc.message)
        }
        return ResponseEntity
            .status(HttpStatus.BAD_REQUEST)
            .header("X-Reason", exc.message)
            .body(exc.message)
    }

    @ExceptionHandler(value=[(HttpMessageNotReadableException::class)])
    fun handleError2(exc: HttpMessageNotReadableException): ResponseEntity<String> {
        return ResponseEntity
            .status(HttpStatus.BAD_REQUEST)
            .header("X-Reason", "Incorrect request body")
            .body("Incorrect request body")
    }
}
