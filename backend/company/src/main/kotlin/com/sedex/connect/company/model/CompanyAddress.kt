package com.sedex.connect.company.model

data class CompanyAddress (
    var addressLine1: String = "",
    var addressLine2: String = "",
    var city: String = "",
    var state: String = "",
    var postalCode: String = "",
    var countryCode: String = ""
)
