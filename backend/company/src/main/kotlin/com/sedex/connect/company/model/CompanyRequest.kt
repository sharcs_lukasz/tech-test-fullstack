package com.sedex.connect.company.model

class CompanyRequest (
    var companyName: String,
    var companyType: String = "",
    var natureofBusiness: String = "",
    var incorporatedDate: String = "",
    var emailAddress: String = "",
    var phoneNumber: String = "",
    var address: CompanyAddress = CompanyAddress()
)
