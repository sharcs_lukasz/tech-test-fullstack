package com.sedex.connect.company.model

import java.util.*

class CompanyResponse (
    var companyName: String,
    var companyType: String,
    var natureofBusiness: String,
    var incorporatedDate: String,
    var emailAddress: String,
    var phoneNumber: String,
    var address: CompanyAddress,
    var createdTime: String,
    var updatedTime: String
){
    val id: String = UUID.randomUUID().toString()
}
