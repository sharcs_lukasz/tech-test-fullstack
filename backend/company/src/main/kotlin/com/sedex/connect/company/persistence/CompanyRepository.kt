package com.sedex.connect.company.persistence

import com.sedex.connect.company.model.CompanyResponse
import java.util.concurrent.ConcurrentHashMap

class CompanyRepository {
    val listOfCompanies : ConcurrentHashMap<String, CompanyResponse> = ConcurrentHashMap<String, CompanyResponse>()

    fun checkIfExits(name: String): Boolean {
        return listOfCompanies.contains(name)
    }

    fun getByName(name: String): CompanyResponse? {
        return listOfCompanies.get(name)
    }

    fun add(company: CompanyResponse): Boolean {
        if(company.companyName.isBlank())
            return false
        if(listOfCompanies.putIfAbsent(company.companyName, company) == null)
            return true
        return false
    }
}
