package com.sedex.connect.company.service

import com.sedex.connect.company.model.CompanyRequest
import com.sedex.connect.company.model.CompanyResponse
import com.sedex.connect.company.repository
import java.time.LocalDateTime

class CompanyService {
    companion object {

        fun addCompany(request: CompanyRequest): CompanyResponse {
            val company = map(request)
            if( repository.add(company) == false )
                throw java.lang.IllegalArgumentException("Company with this name is already registered")
            return company
        }

        fun map(request: CompanyRequest): CompanyResponse {
            if( request.companyName.isBlank())
                throw IllegalArgumentException("Company name cannot be blank")

            val time = LocalDateTime.now().toString()
            return CompanyResponse(
                request.companyName,
                request.companyType,
                request.natureofBusiness,
                request.incorporatedDate,
                request.emailAddress,
                request.phoneNumber,
                request.address,
                time,
                time
            )
        }
    }
}
