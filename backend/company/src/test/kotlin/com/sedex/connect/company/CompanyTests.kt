package com.sedex.connect.company

import com.sedex.connect.company.model.CompanyAddress
import com.sedex.connect.company.model.CompanyRequest
import com.sedex.connect.company.model.CompanyResponse
import com.sedex.connect.company.persistence.CompanyRepository
import com.sedex.connect.company.service.CompanyService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

class CompanyTests {

    fun createTestAddress(): CompanyAddress {
        return CompanyAddress(
            "Tenth Floor",
            "168 Regent St",
            "London",
            "LO",
            "W1B",
            "GB"
        )
    }

    fun createTestCompanyRequest(): CompanyRequest {
        return CompanyRequest(
            "Example Ltd.",
            "limited company",
            "manufacturing",
            "1889-12-01",
            "info@example.net",
            "+44 20 12 34 56 78",
            createTestAddress()
        )
    }

    fun createTestCompany(): CompanyResponse {
        return CompanyResponse(
            "Example Ltd.",
            "limited company",
            "manufacturing",
            "1889-12-01",
            "info@example.net",
            "+44 20 12 34 56 78",
            createTestAddress(),
            "2020-12-18T10:37:09.492814",
            "2020-12-18T10:37:09.492814"
        )
    }

    @Test
    fun companyMapper_HappyPathTest() {
        val company = createTestCompanyRequest()
        val response = CompanyService.map(company)
        assertEquals(company.companyName, response.companyName)
        assertEquals(company.companyType, response.companyType)
        assertEquals(company.natureofBusiness, response.natureofBusiness)
        assertEquals(company.incorporatedDate, response.incorporatedDate)
        assertEquals(company.emailAddress, response.emailAddress)
        assertEquals(company.phoneNumber, response.phoneNumber)
        assertEquals(company.address, response.address)
    }

    @Test
    fun companyMapper_CreatedAndUpdatedTimeEqualForNewObjectsTest() {
        val company = createTestCompanyRequest()
        val response = CompanyService.map(company)
        assertEquals(response.createdTime, response.updatedTime)
    }

    @Test
    fun companyMapper_IdHasUUIDFormatTest() {
        val company = createTestCompanyRequest()
        val response = CompanyService.map(company)
        assertDoesNotThrow { UUID.fromString(response.id) }
    }

    @Test
    fun companyMapper_ThrowsExceptionWhenCompanyNameIsBlank() {
        val company = createTestCompanyRequest()
        company.companyName = " ";
        assertThrows<IllegalArgumentException> { CompanyService.map(company) }
    }

    @Test
    fun companyMapper_CompaniesHaveDifferentUUIDTest() {
        val company1 = createTestCompanyRequest()
        val company2 = createTestCompanyRequest()
        company2.companyName = "Company Inc.";

        val response1 = CompanyService.map(company1)
        val response2 = CompanyService.map(company2)
        assertNotEquals(response1.id, response2.id)
    }

    @Test
    fun companyRepository_DoesNotAddIfAlreadyExistsTest() {
        val company = createTestCompany()
        val companyBis = createTestCompany()

        val repository = CompanyRepository()
        assertTrue(repository.add(company))
        assertFalse(repository.add(companyBis))
    }

    @Test
    fun companyRepository_DoesNotAddIfNameIsBlankTest() {
        val company = createTestCompany()
        company.companyName="   "

        val repository = CompanyRepository()
        assertFalse(repository.add(company))
    }
}
